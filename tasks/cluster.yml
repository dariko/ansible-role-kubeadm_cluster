-   name: get current kubernetes status
    block:
    -   stat:
            path: /etc/kubernetes/pki/ca.crt
        register: out_stat_kubernetes_ca_crt
    -   set_fact:
            kubeadm_done: "{{out_stat_kubernetes_ca_crt.stat.exists}}"

-   name: initialize first node
    when: 
    -   kubeadm_missing_control_nodes|length == kubeadm_cluster_master_nodes|length
    -   inventory_hostname == kubeadm_missing_control_nodes[0]
    block:
    -   name: template kubeadm.conf
        copy:
            content: |
                ---
                {{ kubeadm_cluster_clusterconfig|to_nice_yaml }}
                ---
                {{ kubeadm_cluster_initconfig|to_nice_yaml }}
                ---
                {{ kubeadm_cluster_kubeproxyconfig|to_nice_yaml }}
            dest: /etc/kubeadm.yml
    -   name: kubeadm init
        shell: |
            set -o pipefail
            kubeadm init \
                --config /etc/kubeadm.yml \
                --upload-certs \
                -v=5 \
                2>&1 | tee -a /var/log/kubeadm.log
        args:
            executable: /bin/bash
    -   name: /root/.kube
        file:
            path: /root/.kube
            state: directory
    -   name: copy admin kubeconfig
        copy:
            remote_src: yes
            src: /etc/kubernetes/admin.conf
            dest: /root/.kube/config
    -   k8s:
            definition: "{{ lookup('template', kubeadm_cluster_cni_template) }}"
    -   name: set fact kubeadm_done
        set_fact:
            kubeadm_done: true

-   name: wait for first master to be Ready
    when: inventory_hostname == kubeadm_done_control_nodes[0]
    retries: "{{kubeadm_cluster_init_wait_retries}}"
    delay: 2
    shell: |
        kubectl get nodes
    changed_when: no
    register: out_wait_ready
    until: out_wait_ready.stdout_lines|select('match', '^%s *Ready.*' % kubeadm_cluster_node_name)|list|length > 0
        
-   run_once: yes
    debug:
        var: kubeadm_done_nodes
    
#-   name: read certificates from an initialized control node
    #when:
    #-   kubeadm_done_nodes|length != kubeadm_cluster_nodes|length
    #run_once: yes
    #delegate_to: "{{kubeadm_done_control_nodes[0]}}"
    #slurp:
        #path: "{{item}}"
    #loop:
    ##-   /etc/kubernetes/pki/ca.crt
    #-   /etc/kubernetes/pki/ca.key
    #-   /etc/kubernetes/pki/sa.pub
    #-   /etc/kubernetes/pki/sa.key
    #-   /etc/kubernetes/pki/front-proxy-ca.crt
    #-   /etc/kubernetes/pki/front-proxy-ca.key
    #-   /etc/kubernetes/pki/ca.crt
    #-   /etc/kubernetes/pki/ca.key
    #-   /etc/kubernetes/pki/etcd/ca.crt
    #-   /etc/kubernetes/pki/etcd/ca.key
    #-   /etc/kubernetes/admin.conf
    #register: out_kubernetes_certs

-   name: prepare cluster for new nodes
    when: kubeadm_done_nodes|length != kubeadm_cluster_nodes|length
    include_tasks: generate_token.yml

-   name: get kubeadm_certificate_key
    when: kubeadm_done_control_nodes|length != kubeadm_cluster_nodes|length
    include_tasks: get_kubeadm_certificate_key.yml

-   name: kubeadm upload certs
    when: kubeadm_done_control_nodes|length != kubeadm_cluster_nodes|length
    include_tasks: ensure_kubeadm_certs.yml

-   name: initialize missing nodes
    when:
    -   inventory_hostname not in kubeadm_done_nodes
    block:
    -   name: template kubeadm.conf
        copy:
            content: |
                ---
                {{ kubeadm_cluster_joinconfig|to_nice_yaml }}
            dest: /etc/kubeadm.yml
    -   name: exec kubeadm join
        shell: |
            set -o pipefail
            kubeadm reset -f
            ipvsadm --clear || true
            kubeadm join \
                --config /etc/kubeadm.yml \
                -v=5 \
                2>&1 | tee -a /var/log/kubeadm.log
        args:
            executable: /bin/bash
        retries: 3
        delay: 1

    #-   name: create certificate dirs
        #when:
        #-   inventory_hostname not in kubeadm_done_nodes
        #loop: "{{out_kubernetes_certs.results}}"
        #loop_control:
            #label: "{{item.source|default(None)}}"
        #file:
            #path: "{{item.source|dirname}}"
            #state: directory
    #-   name: write files
        #when:
        #-   inventory_hostname not in kubeadm_done_nodes
        #loop: "{{out_kubernetes_certs.results}}"
        #loop_control:
            #label: "{{item.source|default(None)}}"
        #copy:
            #content: "{{item.content|b64decode}}"
            #dest: "{{item.source}}"


-   name: kubeconfig
    when: inventory_hostname in kubeadm_cluster_master_nodes
    block:
    -   name: /root/.kube
        file:
            path: /root/.kube
            state: directory
    -   name: copy admin kubeconfig
        copy:
            remote_src: yes
            src: /etc/kubernetes/admin.conf
            dest: /root/.kube/config

-   name: wait for nodes to be Ready
    delegate_to: "{{kubeadm_cluster_master_nodes[0]}}"
    retries: "{{kubeadm_cluster_join_wait_retries}}"
    delay: 2
    shell: |
        kubectl get nodes
    register: out_wait_ready
    changed_when: no
    until: out_wait_ready.stdout_lines|select('match', '^%s *Ready.*' % kubeadm_cluster_node_name)|list|length > 0

