# Ansible role to deploy/upgrade kubernetes clusters using kubeadm

## Use

## Upgrade

The `upgrade.yml` file can be executed only explicitely (using the
`include_role`, `tasks_from` ansible keywords).

ex:
`````
-   hosts: nodes
    gather_facts: no
    any_errors_fatal: yes
    tasks:
    -   name: upgrade using kubeadm
        include_role:
            role: kubadm_cluster
            tasks_from: upgrade.yml
`````

The upgrade procedure is as defined in
https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/
and can be summarized as:
1.  on first control node

    1.1. upgrade kubeadm binary

    1.2. drain

    1.3. exec kubeadm upgrade apply $NEW_VERSION

    1.4. uncordon

2.  on remaining control nodes

    2.1 upgrade kubeadm binary

    2.2 drain

    2.3 exec kubeadm upgrade node

    2.4 uncordon

3.  on all control nodes

    3.1 upgrade kubelet binary

    3.2 restart kubelet

4. on all control nodes

    4.1 upgrade kubeadm binary

    4.2 drain

    4.3 exec kubeadm upgrade node

    4.4 uncordon

    4.5 upgrade kubelet binary

    4.6 restart kubelet

The steps {1,2,4}.[1-4] are nearly identical and so have been normalized
in `upgrade_node.yml`, which chooses appropiately between 1.3 and {2,4}.3

After every node has been upgraded by kubeadm the playbook will wait until
there are no pods with `.spec.nodeName==$current_node && .status.phase=="Pending"`.
If `kubeadm_cluster_upgrade_wait` is set to `True` (the default) the
playbook will wait for operator to press a key before moving to the next
node.
